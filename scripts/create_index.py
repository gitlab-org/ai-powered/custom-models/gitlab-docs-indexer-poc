# Some dependencies need to be installed with pip:
# langchain langchain_community langchain_text_splitters

import glob
from langchain.docstore.document import Document
from langchain_text_splitters import MarkdownHeaderTextSplitter
import re
import sqlite3

# path and output path could also be variables passed to the cli

# Assumes it is being run from the gitlab root directory.
path = "doc"

# This will create a local file that can be uploaded. This upload can happen either here using the request library or
# using curl
output_path = "docs.db"

files = glob.glob(path + "/**/*.md", recursive=True)

documents = []

# Read all the files
for filename in files:
    with open(filename, "r") as f:
        doc = Document(
            page_content=f.read(),
            metadata={
                "filename": filename
            }
        )
        documents.append(doc)

# Split content into chunks by its header
headers_to_split_on = [
    ("#", "Header1"),
    ("##", "Header2"),
    ("###", "Header3"),
    ("####", "Header4"),
    ("#####", "Header5"),
]

markdown_splitter = MarkdownHeaderTextSplitter(headers_to_split_on=headers_to_split_on)

rows_to_insert = []

for d in documents:
    md_header_splits = markdown_splitter.split_text(d.page_content)

    for chunk in md_header_splits:
        metadata = {
            **chunk.metadata,
            **d.metadata,
        }
        rows_to_insert.append({"content": chunk.page_content, "metadata": metadata})


# Process each row to yield better results
def build_row_corpus(row):
    corpus = row['content']

    # Remove the preamble
    preamble_start = corpus.find('---')

    if preamble_start != -1:
        preamble_end = corpus.find('---', preamble_start + 1)
        corpus = corpus[preamble_end + 2:-1]

    if not corpus:
        return ''

        # Attach the titles to the corpus, these can still be useful
    corpus = ''.join(row['metadata'].get(f"Header{i}", '') for i in range(1, 6)) + ' ' + corpus

    # Stemming could be helpful, but it is already applied by the sqlite
    # Remove punctuation and set to lowercase, this should reduce the size of the corpus and allow
    # the query to be a bit more robust
    corpus = corpus.lower()
    corpus = re.sub(r'[^\w\s]', '', corpus)
    return corpus


for r in rows_to_insert:
    r['processed'] = build_row_corpus(r)

sql_tuples = [(r['processed'], r['content'], r['metadata']['filename']) for r in rows_to_insert if r['processed']]

# Create the database
conn = sqlite3.connect(output_path)
c = conn.cursor()

c.execute("CREATE VIRTUAL TABLE doc_index USING fts5(processed, content, filename, tokenize='porter trigram');")

c.executemany('INSERT INTO doc_index (processed, content, filename) VALUES (?,?,?)', sql_tuples)

conn.commit()

conn.close()
